
public class Main
{

	public static void main(String[] args) 
	{
		ShipNameProviderImpl provider = new ShipNameProviderImpl(); // This our ShipNameProvider for test purposes
		
		ShipNameGenerator gen = new HorusboydNameGenerator( provider );
		
		for (int i = 0; i < 500; i++) {
			String name = gen.createName();
			provider.addExistingName(name);
			System.out.println(name);
		}
	}

}
