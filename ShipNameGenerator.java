

public interface ShipNameGenerator extends NameGenerator
{
	public void setProvider( ShipNameProvider provider );
	
	public ShipNameProvider getProvider();
}
